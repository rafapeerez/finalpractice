import { Router } from "@vaadin/router";
import '../ui/pages/pages'


const home = document.querySelector('#home');
const router = new Router(home);

router.setRoutes([
    { path: '/', component: 'home-page' },
    // { path: '/user', component: 'user-page' },
    { path: '(.*)', redirect: '/' },
]);