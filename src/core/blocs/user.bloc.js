import { BaseBloc } from './base';

import { GetAllUsersUsecase } from "../usecases/all-user.usecase";
import { AddUserUsecase } from "../usecases/add-user.usecase";


export class UserBloc extends BaseBloc {
    static instance = null;

    static state = {
        users: [],
    }

    constructor() {
        super('User_state');
        UserBloc.instance = this;
    }

    /** 
     * @returns {UserBloc} 
    */
    static getInstance() {
        if (!UserBloc.instance) {
            UserBloc.instance = new UserBloc();
        }
        return UserBloc.instance;
    }

    async getAllUsers(){
        const users = await GetAllUsersUsecase.execute();
        this.setState({
            users
        });
    }

    async addUser(newUser) { 
        const newUsers = await AddUserUsecase.execute(this.getState().users, newUser);
        this.setState({
            users: newUsers
        });
    }
}