import axios from 'axios';

export class UsersRepository { 

    API_URL = 'https://jsonplaceholder.typicode.com/users';

    async getAllUsers() { 
        try {
            const response = await axios.get(API_URL)
            return response.data;
        } catch (error) {
            console.error('Error: Get All Users', error.message);
            throw error;
        }
    }

    async addUser(user) { 
        try {
            const response = await axios.post(API_URL, {
                name: user.name,
                username: user.username,
                address:user.address,
                email: user.email,
                phone: user.phone
            })

            return response.data;
        } catch (error) {
            console.error('Error: Add User', error.message);
            throw error;
        }
    }
}