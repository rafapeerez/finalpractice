
/**
 * @typedef {Object} UserType;
 * @property {String} id;
 * @property {String} username;
 * @property {String} name;
 * @property {String} email;
 * @property {String} address;
 * @property {String} phone;
 */
export class User {
    /**
     * @constructor
     * @param {UserType} userType 
     */
    constructor({ id, username, name, email, address, phone }) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.email = email;
        this.address = address;
        this.phone = phone;
    }
}