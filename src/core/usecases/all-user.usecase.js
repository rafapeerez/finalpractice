import { UsersRepository } from "../repositories/users.repository";
import { User } from "../model/User";

export class GetAllUsersUsecase { 

    /**
     * @returns {Promise<PostType[]>}
     */
    static async execute() { 
        const repository = new UsersRepository();
        const users = await repository.getAllUsers()
        
        return users.map((user) => new User({
            id: user.id,
            username: user.username,
            name: user.name,
            email: user.email,
            address: `Street ${user.address.street}, ${user.address.city}`, 
            phone: user.phone,
        }));
    }
}