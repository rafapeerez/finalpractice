import { UsersRepository } from '../repositories/users.repository';
import { User } from '../model/User'

export class AddUserUsecase { 
    static async execute(users, newUser) {
        const repository = new UsersRepository();
        const responseAPI = await repository.addUser(newUser);

        /**
         * @type {import('../model/User').UserType}
         */
        const userAdded = {
            id: newUser.id,
            name: responseAPI.name,
            username: responseAPI.username,
            address: responseAPI.address,
            email: responseAPI.email,
            phone: responseAPI.phone,
        }

        return [userAdded, ...users];
    }
} 