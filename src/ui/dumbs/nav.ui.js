import { css, html, LitElement } from "lit";
import { AddUserUsecase } from "../../core/usecases/add-user.usecase";
import { User } from "../../core/model/User";

export class NavComponent extends LitElement {

    static get properties() { 
        return {
            page: { type: String },
            formVisible: {type: Boolean}
        };
    }

    static get styles () {
        return css`
            :host {
                display: block;
                border: black solid 2px;
                padding: 0 40px;
                margin-top: 20px;
                margin-bottom: 30px;
            }
            p {
                font-size: 12px;
                margin-right: 80%;
            }
            .container {
                display: flex;
                align-items: center;
            }
            button {
                font-size: 10px;
                height: 30px;
                width: 50px;
                background-color: #35a78a;
                border-radius: 15%;
            }
            button:hover{
                cursor:pointer;
            }
            button:last-child {
                margin-left: auto; 
            }
            #overlay {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.5);
                display: flex;
                justify-content: center;
                align-items: center;
            }
            #modal {
                background-color: white;
                padding: 20px;
                border-radius: 8px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
            }
            #buttonsContainer {
                display: flex;
                justify-content: flex-end; 
                margin-top: 10px;
            }
            label {
                width: 100px; 
                display: inline-block; 
                vertical-align: top; 
                margin-bottom: 5px; 
            }
        `;
    }

    constructor() { 
        super();
        this.formVisible = false;
    }

    openForm() { 
        this.formVisible = true;
    }
    cancelForm() { 
        this.formVisible = false;
    }

    scrollToBottom() { 
        window.scrollTo({
            top: document.body.scrollHeight,
            behavior: 'smooth'
        });
    }

    async submitFormAddUser() { 

        const newUser = new User({
            username: this.shadowRoot.getElementById('username').value,
            name: this.shadowRoot.getElementById('name').value,
            email: this.shadowRoot.getElementById('email').value,
            address: this.shadowRoot.getElementById('address').value,
            phone: this.shadowRoot.getElementById('phone').value,
        });

        const addUserUsecase = new AddUserUsecase();
        const response = await addUserUsecase.execute(newUser);
        const event = new CustomEvent('user-added', {
            detail:{
                username: response.username,
                name: response.name,
                email: response.email,
                address: response.address,
                phone: response.phone,
            }
            
        });
        window.dispatchEvent(event);
        this.formVisible = false;
        this.scrollToBottom();
    }

    render() {
        return html`
            <nav>
                <div class="container">
                    <p>${this.page}</p>
                    ${this.page === "HOME" ? html`
                        <button @click=${() => this.openForm()}>Add</button>
                    ` : html`
                        <button>Update</button>
                        <button>Delete</button>
                    `}
                </div>

                ${this.formVisible ? html`
                    <div id="overlay">
                        <div id="modal">
                            <form id="form">
                                <label for="username">Username:</label>
                                <input type="text" id="username" name="username" required>
                                <br>
                                <label for="name">Name:</label>
                                <input type="text" id="name" name="name" required>
                                <br>
                                <label for="email">Email:</label>
                                <input type="email" id="email" name="email" required>
                                <br>
                                <label for="address">Address:</label>
                                <input type="email" id="address" name="address" value="Street " required> 
                                <br>
                                <label for="phone">Phone:</label>
                                <input type="tel" id="phone" name="phone" required>
                                <br>
                                <div id="buttonsContainer">
                                    <button @click=${() => this.cancelForm()}>Cancel</button>
                                    <button @click=${() => this.submitFormAddUser()}>Submit</button>
                                </div>                                
                            </form>
                        </div>
                    </div>
                ` : ''}
            </nav>
        `;
    }
}

customElements.define('nav-component', NavComponent);
