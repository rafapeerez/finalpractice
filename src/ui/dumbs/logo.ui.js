import { css, html, LitElement } from "lit";

export class LogoComponent extends LitElement {
    static get styles () {
        return css`
            p {
                display: block;
                border: black solid 2px;
                width: fit-content;
                padding: 0.5% 3%;
            }
        `;
    }

    render() {
        return html`
            <header>
                <p>LOGO</p>
            </header>
        `;
    }
}

customElements.define('logo-component', LogoComponent);
