import { LitElement, html } from "lit";
import '../dumbs/dumbs'
import '../smarts/smarts'
import { UserBloc } from "../../core/blocs/user.bloc";

export class HomePage extends LitElement {

    async connectedCallback() { 
        super.connectedCallback();
        const usersBloc = UserBloc.getInstance();
        if (!usersBloc.getState() || usersBloc.getState().users.length == 0) { 
            await usersBloc.getAllUsers();
        }
    }

    render() { 
        return html`
            <logo-component></logo-component>
            <nav-component page="HOME"></nav-component>
            <userdetails-component></userdetails-component>
            <usercards-component></usercards-component>
        `;
    }
}

customElements.define('home-page', HomePage);