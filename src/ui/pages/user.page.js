import { LitElement, html } from "lit";
import '../dumbs/dumbs'


export class UserPage extends LitElement { 
    
    /* static get properties() {
        return {
            user: {
                type: User
            }
        };
    }

    async connectedCallback() { 
        super.connectedCallback();
        window.addEventListener('show-details', this.handleShowDetails);
    }

    handleShowDetails = (event) => { 
        const userDetails = {
            username: event.detail.username,
            name: event.detail.name,
            email: event.detail.email,
            address: event.detail.address,
            phone: event.detail.phone,
        }
        this.user = userDetails;
        console.log(this.user);
    } */


    render() { 
        return html`
            <logo-component></logo-component>
            <nav-component page="USER"></nav-component>
            <userdetails-component></userdetails-component>
        `;
    }
}

customElements.define('user-page', UserPage);