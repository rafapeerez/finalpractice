import { css, html, LitElement } from "lit";
import { GetAllUsersUsecase } from "../../core/usecases/all-user.usecase";

export class UserDetailsComponents extends LitElement {

    static get styles() {
        return css`
            .card {
                border: 1px solid #ccc;
                border-radius: 8px;
                padding: 10px;
                margin-top: 1%;
                width: 90%;
                height: 250px;
                background-color: #beffef;
                box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
                margin-left: 3%;
                margin-bottom: 5%;
            }
            .emptyCard{
                border: 1px solid #ccc;
                border-radius: 8px;
                padding: 10px;
                margin-top: 1%;
                width: 90%;
                height: 100px;
                background-color: #beffef;
                box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
                margin-left: 3%;
                margin-bottom: 5%;
            }
            h1 {
                font-size: 30px;
            }
            h2 {
                font-size: 16px;
            }
            h3 {
                margin-left: 3%;
                margin-top: 4%;
            }
            button {
                font-size: 10px;
                height: 30px;
                width: 50px;
                background-color: #35a78a;
                border-radius: 15%;
                margin-left: 1%;
            }
            button:hover{
                cursor:pointer;
            }
            #buttonsContainer {
                display: flex;
                justify-content: flex-end;
            }
        `;
    }

    static get properties() {
        return {
            users: { type: Array },
            userDetail: { type: Object }
        };
    }

    constructor() {
        super();
        this.userDetail = {};
    }

    async connectedCallback() { 
        super.connectedCallback();
        this.users = await GetAllUsersUsecase.execute();

        window.addEventListener('show-details', this.handleShowDetails);
    }

    handleShowDetails = (event) => { 
        const userDetails = {
            username: event.detail.username,
            name: event.detail.name,
            email: event.detail.email,
            address: event.detail.address,
            phone: event.detail.phone,
        }
        this.userDetail = userDetails;
        this.requestUpdate();
    }

    render() {
        return html`
            <h3>Detail Information</h3>
            ${Object.keys(this.userDetail).length > 0 ? html`
                <div class="card">
                    <h1>${this.userDetail.username}</h1>
                    <h2>Name: ${this.userDetail.name}</h2>
                    <h2>Email: ${this.userDetail.email}</h2>
                    <h2>Address: ${this.userDetail.address}</h2>
                    <h2>Phone: ${this.userDetail.phone}</h2>
                    <div id="buttonsContainer">
                        <button>Update</button>
                        <button>Delete</button>
                    </div>
                </div>
            ` : html`
                <div class="emptyCard">
                    <h3>Select a user...</h3>
                </div>
            `}            
        `;
    }
}

customElements.define('userdetails-component', UserDetailsComponents);
