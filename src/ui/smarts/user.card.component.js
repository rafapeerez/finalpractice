import { css, html, LitElement } from "lit";
import { UserBloc } from "../../core/blocs/user.bloc.js";

export class UserCardsComponents extends LitElement {

    static get styles() {
        return css`
            .card {
                border: 1px solid #ccc;
                border-radius: 8px;
                padding: 10px;
                margin-right: 2%;
                margin-top: 2%;
                width: 90%;
                background-color: #beffef;
                box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
                display: inline-block;
            }
            .card:hover {
                background-color: #35a78a;
                cursor: pointer;
            }
            .container {
                margin-left: 2%;
                column-count: 2;
                column-gap: 20px; 
                margin-bottom: 5%;
            }
            h1 {
                font-size: 20px;
            }
            h2 {
                font-size: 12px;
            }
            h3 {
                margin-left: 3%;
                margin-top: 4%;
            }
        `;
    }

    connectedCallback() { 
        super.connectedCallback();
        this.usersBloc = UserBloc.getInstance();
        this.handleState = (state) => { 
            this.users = state.users;
            this.requestUpdate();
        }
        this.usersBloc.subscribe(this.handleState);
    }

    disconnectedCallback() { 
        super.disconnectedCallback();
        this.usersBloc.unsubscribe(this.handleState);
    }

    scrollToTop() { 
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    }

    // showDetails(user) { 
    //     const event = new CustomEvent('show-details', {
    //         bubbles: true,
    //         composed: true,
    //         detail:{
    //             username: user.username,
    //             name: user.name,
    //             email: user.email,
    //             address: user.address,
    //             phone: user.phone,
    //         }
    //     });
    //     window.dispatchEvent(event);
    //     this.scrollToTop();
    // }

    render() {
        return html`
            <h3>Users</h3>
            <div class="container"> 
                ${this.users?.map((user) => html`
                    <div class="card" id="user_${user.id}" @click=${() => this.showDetails(user)}>
                        <h1>${user.username}</h1>
                        <h2>Name: ${user.name}</h2>
                    </div>
                `)}
            </div>
        `;
    }

}

customElements.define('usercards-component', UserCardsComponents);
