import { UsersRepository } from "../../src/core/repositories/users.repository";
import { UserBloc } from "../../src/core/blocs/user.bloc";
import USERS from "../fixtures/users.json"

jest.mock("../../src/core/repositories/users.repository")

describe("All user use case", () => {

    beforeEach(() => { 
        UsersRepository.mockClear();
    });
    
    it("should list all users", async () => {

        UsersRepository.mockImplementation(() => { 
            return {
                getAllUsers: () => {
                    return USERS;
                },
            };
        });

        const usersBloc = UserBloc.getInstance();
        await usersBloc.getAllUsers();


        expect(usersBloc.getState().users.length).toBe(USERS.length);
    });
})