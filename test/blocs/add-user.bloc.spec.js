import { UserBloc } from "../../src/core/blocs/user.bloc";
import { User } from "../../src/core/model/User";
import { UsersRepository } from "../../src/core/repositories/users.repository";
import USERS from "../fixtures/users.json"
import { v4 } from "uuid";

jest.mock("../../src/core/repositories/users.repository")

describe("Add user use case", () => {

    beforeEach(() => { 
        UsersRepository.mockClear();
    });
    
    it("should create an user", async () => {

        /**
         * @type {import("../../src/core/model/User").UserType}
         */
        const newUser = new User({
            id: v4(),
            username: "Rafael",
            name: "Prueba",
            email: "Prueba",
            address: "Prueba", 
            phone: "2222222222",
        })

        UsersRepository.mockImplementation(() => { 
            return {
                addUser: jest.fn().mockResolvedValue({
                    id: 101,
                    username: newUser.username,
                    name: newUser.name,
                    email: newUser.email,
                    address: newUser.address, 
                    phone: newUser.phone,
                }),
                getAllUsers: jest.fn().mockResolvedValue(USERS),
            }
        });

        const userBloc = UserBloc.getInstance();
        await userBloc.getAllUsers();
        const usersBeforeAdd = userBloc.getState().users;

        await userBloc.addUser(newUser);
        const usersAfterAdd = userBloc.getState().users;
        
        expect(usersAfterAdd.length).toBe(usersBeforeAdd.length + 1);
        expect(usersAfterAdd[0].username).toBe(newUser.username);
        expect(usersAfterAdd[0].name).toBe(newUser.name);
        
    });
})